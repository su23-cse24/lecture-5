#include <igloo/igloo.h>
#include "func.h"

using namespace igloo;


Context(TestLengthOfLongestSubstring){
    Spec(test_1){
        Assert::That(lengthOfLongestSubstring("abcabcc"), Equals(3));
    }
    Spec(test_2){
        Assert::That(lengthOfLongestSubstring("bbbbb"), Equals(1));
    }
    Spec(test_3){
        Assert::That(lengthOfLongestSubstring(""), Equals(0));
    }
    Spec(test_4){
        Assert::That(lengthOfLongestSubstring("abcd"), Equals(4));
    }
    Spec(test_5){
        Assert::That(lengthOfLongestSubstring("12"), Equals(2));
    }
    Spec(test_6){
        Assert::That(lengthOfLongestSubstring("pwwkew"), Equals(3));
    }
    Spec(test_7){
        Assert::That(lengthOfLongestSubstring("dvdf"), Equals(3));
    }
    Spec(test_8){
        Assert::That(lengthOfLongestSubstring("dacevdf"), Equals(6));
    }
};

int main(int argc, const char* argv[]){
    TestRunner::RunAllTests(argc, argv);
}
