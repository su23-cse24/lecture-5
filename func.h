#ifndef FUNC_H
#define FUNC_H

#include <iostream>
#include <vector>
#include <string>

// abcabcc => 3
// abcd => 4
// dvdf => 3
// pwwkew => 3

// letters: [w, k]
// index: 0
// longest: 3

// check if a target letter is inside of the vector v
int searchVector(std::vector<char>& v, char target) {
    for (int i = 0; i < v.size(); i++) {
        if (v[i] == target) {
            return i;
        }
    }
    return -1;
}

int lengthOfLongestSubstring(std::string s) {
    std::vector<char> letters;
    int longest = 0;

    for (int i = 0; i < s.length(); i++) {
        int index = searchVector(letters, s[i]);
        if (index == -1) {
            letters.push_back(s[i]);
        } else {
            for (int i = 0; i <= index; i++) {
                letters.erase(letters.begin());
            }
            letters.push_back(s[i]);
        }

        if (letters.size() > longest) {
            longest = letters.size();
        }
    }

    return longest;
}

// int lengthOfLongestSubstring(std::string s) {
//     std::vector<char> letters;
//     int longest = 0;
//     int count = 0;

//     for (int i = 0; i < s.length(); i++) {
//         int j = i;
//         count = 0;
//         letters.clear();
//         while (!searchVector(letters, s[j]) && j < s.length()) {
//             letters.push_back(s[j]);
//             count++;
//             j++;
//         }

//         if (count > longest) {
//             longest = count;
//         }
//     }

//     if (count > longest) {
//         longest = count;
//     }

//     return longest;
// }

#endif