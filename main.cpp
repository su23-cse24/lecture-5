#include <iostream>
#include "func.h"
using namespace std;

/*
    Given a string s, find the length of the longest substring without repeating characters.

    Examples: 
    s = "abcabcc"
    Output: 3
    Explanation: "abc" is the longest substring without repeating characters
*/

int main() {
    string s = "pwwkew";

    cout << lengthOfLongestSubstring(s) << endl;

    return 0;
}